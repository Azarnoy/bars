package com.dmitriy.azarenko.thisifbars;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {

    TextView info;
    String comeInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        info = (TextView) findViewById(R.id.info_text_id);

        comeInfo = getIntent().getExtras().getString("info");
        info.setText(comeInfo);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


//         set the buttons for menu
        switch (id) {
            case R.id.action_map_but:
                Intent intent = new Intent(InfoActivity.this, ActivityMap.class);
                startActivity(intent);

                return true;
            case R.id.action_listOfMap_but:
                Intent intentList = new Intent(InfoActivity.this, MainActivity.class);
                startActivity(intentList);

                return true;
            case R.id.action_busy_id:

                return true;
            case R.id.action_settings:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
