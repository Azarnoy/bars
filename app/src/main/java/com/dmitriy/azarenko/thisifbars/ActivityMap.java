package com.dmitriy.azarenko.thisifbars;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

public class ActivityMap extends AppCompatActivity {
    private static final String TAG = "tag";

    final LatLng KHARKOV = new LatLng(49.9944422, 36.2368201);
    int PLACE_PICKER_REQUEST = 1;
    Button btReturn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_map);


//        set Place Picker
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {

            startActivityForResult(builder.build(ActivityMap.this), PLACE_PICKER_REQUEST);
            Log.d(TAG, "start Place Picker");



        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

        btReturn = (Button) findViewById(R.id.button_return_id);
        btReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {

                    startActivityForResult(builder.build(ActivityMap.this), PLACE_PICKER_REQUEST);


                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    //      set the data from choose place to info activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);


                String toastMsg = String.format(
                        "Place: %s \n" +
                                "Adress: %s \n" +
                                "Phone number: %s \n" +
                                "Web site: %s \n",
                        place.getName(), place.getAddress(), place.getPhoneNumber(), place.getWebsiteUri());
                //TODO if web site null
                Intent intent = new Intent(ActivityMap.this, InfoActivity.class);
                intent.putExtra("info", toastMsg);
                startActivity(intent);
                Log.d(TAG, "set data from PP to infoActivity");


            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //  buttons for menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_map_but:
                Intent intent = new Intent(ActivityMap.this, ActivityMap.class);
                startActivity(intent);


                return true;
            case R.id.action_listOfMap_but:
                Intent mintent = new Intent(ActivityMap.this, MainActivity.class);
                startActivity(mintent);

                return true;
            case R.id.action_busy_id:

                return true;
            case R.id.action_settings:

                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

}
