package com.dmitriy.azarenko.thisifbars;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceFilter;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "tag";

    private GoogleApiClient mGoogleApiClient;

    TrackerService trackerService;
    Intent serviceIntent;


    ArrayList<String> listForAd = new ArrayList<>();
    ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serviceIntent = new Intent(this, TrackerService.class);



        ListView listView = (ListView) findViewById(R.id.listView);

        adapter = new ArrayAdapter<String>(this, R.layout.list_item, listForAd);
        listView.setAdapter(adapter);
//       set click on list view items
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
//     set google places
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        List<String> filterType = new ArrayList<>();
        filterType.add(Integer.toString(Place.TYPE_CAFE));
        PlaceFilter filter = new PlaceFilter(false, filterType);





        PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi
                .getCurrentPlace(mGoogleApiClient, null);
//get names from PlacePicker and put in list
        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @Override
            public void onResult(PlaceLikelihoodBuffer likelyPlaces) {
                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                    Log.i(TAG, String.format("Place '%s' has likelihood: %g",

                            placeLikelihood.getPlace().getName(),
                            placeLikelihood.getLikelihood()));

                    listForAd.add(String.valueOf(placeLikelihood.getPlace().getName()));
                    Log.d(TAG, "add data in list");


                }

                adapter.notifyDataSetChanged();
                likelyPlaces.release();
            }
        });


    }

    //        create menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


//         set the buttons for menu
        switch (id) {
            case R.id.action_map_but:
                Intent intent = new Intent(MainActivity.this, ActivityMap.class);
                startActivity(intent);
                trackerService.startTracking();

                return true;
            case R.id.action_listOfMap_but:
                Toast.makeText(MainActivity.this, "You are now in List menu", Toast.LENGTH_SHORT).show();

                return true;
            case R.id.action_busy_id:
                Toast.makeText(MainActivity.this, "In developing", Toast.LENGTH_SHORT).show();

                return true;
            case R.id.action_settings:
                Toast.makeText(MainActivity.this, "In developing", Toast.LENGTH_SHORT).show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        startService(serviceIntent);
        bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!trackerService.isTracking()) {
            stopService(serviceIntent);
        }
        unbindService(serviceConnection);
    }

    @Override
    protected void onDestroy() {
        trackerService.stopTracking();
        super.onDestroy();
    }

    private void updateStatus() {
        if (trackerService.isTracking()) {
            Toast.makeText(MainActivity.this, "Tracking enabled" + trackerService.getLocationsCount(), Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Tracking enabled");
        } else {
            Toast.makeText(MainActivity.this, "Tracking not enabled", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Tracking not enabled");
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            trackerService = ((TrackerService.TrackerBinder) service).getService();
            updateStatus();
            Log.d(TAG, "start service");
        }

        public void onServiceDisconnected(ComponentName className) {
            trackerService = null;
        }
    };


}